package org.example;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class WriteToSql {


    private WriteToSql() {
        throw new IllegalStateException("Utility class");
    }

    static String userName = "root";
    static String password = "09052001";
    static String jdbcUrl = "jdbc:mysql://localhost:3306/mydata";
    static String insertQuery = "INSERT INTO student (AdmissionNumber, Name, Physics, Chemistry, Maths) VALUES (?, ?, ?, ?, ?)";
    static String searchQuery = "SELECT * FROM student WHERE AdmissionNumber = ?";
    static String searchNameQuery = "SELECT * FROM student WHERE Name = ?";


    private static void deleteSql() {
        try (Connection connection = DriverManager.getConnection(jdbcUrl, userName, password);
             PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM student");) {

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void writeToSql(List<Student> students) {
        try (Connection connection = DriverManager.getConnection(jdbcUrl, userName, password);
             PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);) {

            deleteSql();
            for (Student student : students) {
                int index = 1;
                for (String value : student.studentDetails.values()) {
                    preparedStatement.setString(index++, value);
                }
                preparedStatement.executeUpdate();
            }

            System.out.println("Updated SQL database...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static Scanner scanner = new Scanner(System.in);

    public static void searchInSql() {

        System.out.println("Search in database using");
        System.out.println("1. Admission no.");
        System.out.println("2. Student Name.");
        System.out.println("Enter Choice : ");

        int choice = scanner.nextInt();
        switch (choice) {
            case 1 -> idSearch();
            case 2 -> nameSearch();
            default -> System.out.println("Invalid Choice");
        }

    }

    public static void idSearch() {
        System.out.println("enter id to be searched : ");
        String id = scanner.next();
        try (Connection connection = DriverManager.getConnection(jdbcUrl, userName, password);
             PreparedStatement preparedStatement = connection.prepareStatement(searchQuery);) {

            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();


            Student student = new Student();
            // Iterate through the result set and convert each row to JSON
            while (resultSet.next()) {

                student.writeData("AdmissionNumber", resultSet.getString("AdmissionNumber"));
                student.writeData("Name", resultSet.getString("Name"));
                student.writeData("Physics", resultSet.getString("Physics"));
                student.writeData("Chemistry", resultSet.getString("Chemistry"));
                student.writeData("Maths", resultSet.getString("Maths"));

            }
            ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
            String studentResultJson = objectMapper.writeValueAsString(student.studentDetails);
            System.out.println(studentResultJson);

            // Close the resources
            resultSet.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void nameSearch() {

        System.out.println("enter name to be searched : ");
        String Name = scanner.next();
        try (Connection connection = DriverManager.getConnection(jdbcUrl, userName, password);
             PreparedStatement preparedStatement = connection.prepareStatement(searchNameQuery);) {

            preparedStatement.setString(1, Name);
            ResultSet resultSet = preparedStatement.executeQuery();

            List<Student> students = new ArrayList<>();
            while (resultSet.next()) {

                Student student = new Student();
                student.writeData("admission_number", resultSet.getString("AdmissionNumber"));
                student.writeData("Name", resultSet.getString("Name"));
                student.writeData("Physics", resultSet.getString("Physics"));
                student.writeData("Chemistry", resultSet.getString("Chemistry"));
                student.writeData("Maths", resultSet.getString("Maths"));
                students.add(student);

            }
            ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
            List<Object> jsonObjectList = new ArrayList<>();

            for (Student student : students) {
                String jsonData = objectMapper.writeValueAsString(student.studentDetails);
                Object json = objectMapper.readValue(jsonData, Object.class);
                jsonObjectList.add(json);

            }
            String jsonArray = objectMapper.writeValueAsString(jsonObjectList);
            System.out.println(jsonArray);
            resultSet.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
