package org.example;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Student> students = new ArrayList<>();
        String dataPath = "Student.xlsx";

//read data from excel
        try(FileInputStream file = new FileInputStream(dataPath);
            Workbook workSheet = new XSSFWorkbook(file))
        {

            Sheet sheet = workSheet.getSheetAt(0);
            int lastRowIndex = sheet.getLastRowNum();

            for (int rowIndex=1 ;rowIndex<=lastRowIndex ;rowIndex++){
                Row row = sheet.getRow(rowIndex);
                Student student = new Student();
                for (Cell cell:row){
                    String data;
                    String header;
                    data = cell.toString();
                    header = sheet.getRow(0).getCell(cell.getColumnIndex()).getStringCellValue();
                    student.writeData(header,data);

                }
                students.add(student);
            }

        }catch (IOException e){
            e.printStackTrace();
        }

//mapping to json
        ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        List<Object> jsonObjectList = new ArrayList<>();
        try(FileWriter fileWriter = new FileWriter("student.json");) {
            for (Student student:students) {
                String jsonData= objectMapper.writeValueAsString(student.studentDetails);
                Object json = objectMapper.readValue(jsonData,Object.class);
                jsonObjectList.add(json);

            }

            String jsonArray = objectMapper.writeValueAsString(jsonObjectList);


            fileWriter.write(jsonArray);


        }catch (IOException e){
            e.printStackTrace();
        }


// write to sql
        System.out.println("Writing data to SQL table students...");
        WriteToSql.writeToSql(students);

        WriteToSql.searchInSql();

    }
}
