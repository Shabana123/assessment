public class Subject {
    private String name;
    private double marks;
    private String grade;
    private double gradePoint;

    public Subject(String name, double chem, String grade, double gradePoint) {
        this.name = name;
        this.marks = chem;
        this.grade = grade;
        this.gradePoint = gradePoint;
    }

    public String getName() {
        return name;
    }

    public double getMarks() {
        return marks;
    }

    public String getGrade() {
        return grade;
    }

    public double getGradePoint() {
        return gradePoint;
    }

    @Override
    public String toString() {
        return name + ":\n\tMark: " + marks + "\n\tGrade: " + grade + "\n\tGrade Point: " + gradePoint;
    }
}
