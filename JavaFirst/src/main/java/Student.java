
import java.util.List;
public class Student {
    private String admissionNumber;
    private String name;
    private List<Subject> subjects;
    private double percentage;
    private int totalMarks;

    public Student(String admissionNumber, String name, List<Subject> subjects) {
        this.admissionNumber = admissionNumber;
        this.name = name;
        this.subjects = subjects;
        calculatePercentageAndTotalMarks();
    }

    private void calculatePercentageAndTotalMarks() {
        int totalMarks = 0;
        for (Subject subject : subjects) {
            totalMarks += subject.getMarks();
        }
        this.totalMarks = totalMarks;
        this.percentage = (double) totalMarks / (subjects.size() * 100) * 100;
    }

    public String getAdmissionNumber() {
        return admissionNumber;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: ").append(name).append("\n");
        sb.append("Admission No: ").append(admissionNumber).append("\n");
        sb.append("Percentage: ").append(percentage).append("\n");
        for (Subject subject : subjects) {
            sb.append(subject).append("\n");
        }
        return sb.toString();
    }
}
