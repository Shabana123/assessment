
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Main {

    private static List<Student> students = new ArrayList<>();

    public static void main(String[] args) {

        String excelFilePath = "Student.xlsx";

        try (FileInputStream fis = new FileInputStream(excelFilePath);

             Workbook workbook = new XSSFWorkbook(fis)) {

            Sheet sheet = workbook.getSheetAt(0);

            ArrayList<String> sub = new ArrayList<String>();


            for (int rowIndex = 0; rowIndex < 1; rowIndex++) {

                Row row = sheet.getRow(rowIndex);

                if (row == null) {

                    continue;

                }

                for (int x = 2; x <= 4; x++) {

                    String phy = row.getCell(x).getStringCellValue();

                    sub.add(phy);

                }

            }

            for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
                Row row = sheet.getRow(rowIndex);

                if (row == null) {
                    continue;

                }
                String admissionNumber = getCellValueAsString(row.getCell(0));
                String name = row.getCell(1).getStringCellValue();

                List<Subject> subject = new ArrayList<>();

                for (int x = 2; x <= 4; x++) {
                    double phy = row.getCell(x).getNumericCellValue();
                    String g_phy = assignGrades(phy);
                    double gp_phy = assignGradePoints(phy);
                    subject.add(new Subject(sub.get(x - 2), phy, g_phy, gp_phy));


                }


                Student student = new Student(admissionNumber, name, subject);
                students.add(student);


            }

        } catch (IOException e) {

            e.printStackTrace();

        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Search student by:");

        System.out.println("1. Admission Number");

        System.out.println("2. Name");

        System.out.print("Enter your choice: ");

        int choice = scanner.nextInt();

        switch (choice) {

            case 1:
                System.out.print("Enter Admission Number: ");
                String admissionNumber = scanner.next();

                searchAndDisplayStudentByAdmissionNumber(admissionNumber);

                break;


            case 2:

                System.out.print("Enter Name: ");

                String name = scanner.next();
                searchAndDisplayStudentsByName(name);
                break;


            default:
                System.out.println("Invalid choice.");

        }


    }


    public static String assignGrades(double m) {

        String grades;

        if (m >= 91) {

            grades = "A1";

        } else if (m >= 81) {

            grades = "A2";

        } else if (m >= 71) {

            grades = "B1";

        } else if (m >= 61) {

            grades = "B2";

        } else if (m >= 51) {

            grades = "C1";

        } else if (m >= 41) {

            grades = "C2";

        } else if (m >= 33) {

            grades = "D";

        } else if (m >= 21) {

            grades = "E1";

        } else {

            grades = "E2";

        }

        return grades;

    }


    public static double assignGradePoints(double m) {

        double gradePoints;
        if (m >= 91) {

            gradePoints = 10.0;

        } else if (m >= 81) {

            gradePoints = 9.0;

        } else if (m >= 71) {

            gradePoints = 8.0;

        } else if (m >= 61) {

            gradePoints = 7.0;

        } else if (m >= 51) {

            gradePoints = 6.0;

        } else if (m >= 41) {

            gradePoints = 5.0;

        } else if (m >= 33) {

            gradePoints = 4.0;

        } else {

            gradePoints = 0;

        }

        return gradePoints;

    }


    private static String getCellValueAsString(Cell cell) {
        if (cell == null) {
            return "";

        }

        CellType cellType = cell.getCellType();


        if (cellType == CellType.STRING) {

            return cell.getStringCellValue().trim();

        } else if (cellType == CellType.NUMERIC) {

            return String.valueOf((int) cell.getNumericCellValue()).trim();

        } else {

            return "";

        }

    }


    private static void searchAndDisplayStudentByAdmissionNumber(String admissionNumber) {

        for (Student student : students) {
            if (student.getAdmissionNumber().equals(admissionNumber)) {

                System.out.println(student);

                return;

            }

        }


        System.out.println("Student with Admission Number " + admissionNumber + " not found.");

    }


    private static void searchAndDisplayStudentsByName(String name) {


        int foundCount = 0;


        for (Student student : students) {

            if (student.getName().equals(name)) {

                System.out.println(student);

                foundCount++;

            }

        }

        if (foundCount == 0) {

            System.out.println("Student with Name " + name + " not found.");

        }

    }

}

